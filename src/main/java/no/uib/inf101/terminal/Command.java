package no.uib.inf101.terminal;

import java.util.Map;

public interface Command {
    String run(String[] args);
    String getName();
    default void setContext(Context context)
    {

    }

    
    String getManual();
    
    
}
