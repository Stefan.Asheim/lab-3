package no.uib.inf101.terminal;

public class CmdEcho implements Command {

    @Override
    public String getName()
    {
        return "echo";
    }

    @Override
    public String run(String[] cmds)
    {
        String FinalString = "";
        for(String cmd : cmds)
        {
            FinalString = FinalString + cmd + " ";
        }
        return FinalString;
    }

    @Override
    public String getManual() {
        return "Prints out an echo of what you entered";
    }
}
